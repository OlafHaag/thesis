\documentclass[stu,a4paper,floatsintext,donotrepeattitle]{apa7} % Use draftall option during write-up. Maybe follow recommendation with 10pt Computer Modern

\usepackage{lipsum}

\usepackage[ngerman,american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[calc,en-US]{datetime2}

\usepackage{csquotes}
\usepackage[style=apa,sortcites=true,sorting=nyt,backend=biber]{biblatex}
\DeclareLanguageMapping{american}{american-apa}
\addbibresource{bibliography.bib}
\usepackage[Export]{adjustbox}
\usepackage{graphicx}
\graphicspath{{res/}{../res/}{res/img/}{../res/img/}{../../res/img/}{res/reports/}{res/reports/figures/}{../res/reports/figures/}{../res/reports/}}
\usepackage{wrapfig}
\usepackage{ccicons}
\usepackage{pdfpages}
\usepackage{subfiles}
\usepackage{subcaption}

\usepackage{booktabs}
\usepackage{datatool}
% Start every dtl table with \toprule from booktabs. Likewise for \midrule and \bottomrule from booktabs.
\renewcommand{\dtldisplaystarttab}{\toprule}
\renewcommand{\dtldisplayafterhead}{\midrule}
\renewcommand{\dtldisplayendtab}{\\\bottomrule}

\usepackage{siunitx}
% New command to convert decimal values to percent.
\newcommand\percentage[2][round-precision = 2]{% default precision: 2
    \SI[round-mode = places,
        scientific-notation = fixed, fixed-exponent = 0,
        output-decimal-marker={.}, #1]{#2e2}{\percent}%
}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{array}
\newcolumntype{H}{>{\setbox0=\hbox\bgroup}c<{\egroup}@{}} % Hidden column.
\usepackage{csvsimple}
\usepackage[final]{showkeys} % ToDo: "draft" option during write-up. Set showkeys to final when done.
\usepackage[capitalise]{cleveref}
\usepackage{multirow}

% Projection operator for equations.
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclareMathOperator{\proj}{proj}
\newcommand{\vct}{\mathbf}
\newcommand{\vctproj}[2][]{\proj_{\vct{#1}}\vct{#2}}

% Numbered environment for hypotheses.
\newtheorem{hypothesis}{Hypothesis}[subsubsection]
% I only define hypotheses in subsubsections of a subsection. I don't want all the upper section hierarchy numbers displayed.
\renewcommand{\thehypothesis}{\arabic{subsubsection}.\arabic{hypothesis}}
\crefname{hypothesis}{hypothesis}{hypotheses}
\Crefname{hypothesis}{Hypothesis}{Hypotheses}

% Numbered environment for Bayesian models.
\newcounter{modelCounter}
\setcounter{modelCounter}{-1}
\makeatletter
\newenvironment{model}
    {
      \refstepcounter{modelCounter}%
      \label{model:\themodelCounter}
      \centering
      \begin{minipage}[c]{\textwidth}
          \centering
          Model: \themodelCounter\\[1ex]
          \begin{tabular}{|p{\textwidth}|}
            \hline
            $$
              \begin{array}{rcl}
    }
    { 
              \end{array}
            $$
            \\\hline
          \end{tabular} 
      \end{minipage}
    }
\makeatother
\crefname{modelCounter}{model}{models}
\Crefname{modelCounter}{Model}{Models}

\makeatletter
\providecommand*{\input@path}{}
\g@addto@macro\input@path{{./sections//}{./sections/appendix//}{./appendix//}}
\makeatother

\title{Effects of Secondary Constraints on Bimanual Two-Digit Synergies in an Accurate Reaching Task}
\shorttitle{Synergies in bimanual finger reaching task}

\author{Olaf Haag\\Matrikel-Nr.: 2339927}
\affiliation{Department of Psychology, Philipps-Universität Marburg}
\course{Diplomarbeit}
\professor{\underline{Supervisors:}\\Prof. Dr. Dominik Endres\\Prof. Dr. Anna Schubö}
\duedate{October 2020}

\leftheader{Haag}

\usepackage[acronym,toc,automake]{glossaries-extra}
\makeglossaries
\subfile{sections/glossary}

% ToDo ! abstract
\abstract{
The human musculoskeletal system has more degrees of freedom (e.g. joints, muscles) than necessary to accomplish most everyday tasks and is in that sense a redundant system \parencite{bernstein1967}.
The involved elements have been found to covariate in such a way that variance is confined to a subspace that yields the same task performance \parencite{Scholz1999}.
This leads to a flexibility that allows to fulfill concurrent tasks by configuring the involved degrees of freedom in directions that do accomplish each task at the same time.
An open question is whether a secondary task interferes with the task performance of the first task.
This study aims to investigate this issue.
Optimal feedback control theory \parencite{Todorov2004} predicts that task performance decreases when the variance in the redundant subspace is narrowed by a different motor control scheme due to secondary constraints.
The uncontrolled manifold hypothesis predicts no such increase in variability in task performance by a secondary task \parencite{Zhang2008,Danion2011}.
A novel motor task on mobile devices was performed by 44 participants aged from under 20 years old to over 60 years old, from which 30 were included in the statistical analysis.
Bayesian analyses of the variance components that do affect task performance and those that yielded inconclusive results.
An influence of instructional material and visual stimuli on the employed motor strategy was found which could be attributed to an increased extraneous cognitive load or split attention effects \parencite{Chandler1991}.

%   State the problem under investigation, including main hypotheses.
%   Describe subjects (nonhuman animal research) or participants (human research), specifying their pertinent characteristics for the study; Participants are described in greater detail in the body of the paper. Describe the study method, including ‒ research design (e.g., experiment, observational study)
% ‒ sample size
% ‒ materials used (e.g., instruments, apparatus)
% ‒ outcome measures
% ‒ data-gathering procedures, including a brief description of the source of any secondary data. If the study is a secondary data analysis, so indicate.
% Report findings, including effect sizes and confidence intervals or statistical significance levels.
% State conclusions, beyond just results, and report the implications or applications.
}

\keywords{motor control, redundancy, uncontrolled manifold, optimal feedback control}

\authornote{

  Correspondence concerning this thesis should be addressed to Olaf Haag, 
	E-mail: contact@olafhaag.com}

\begin{document}

% Hides authornote and abstract from table of content.
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
\maketitle
\addtocontents{toc}{\protect\setcounter{tocdepth}{3}}

\tableofcontents
\pagebreak

\subfile{sections/introduction}

\subfile{sections/theory}

\clearpage
\subfile{sections/methods}

\clearpage
\subfile{sections/results}

\clearpage
\subfile{sections/discussion}

\subfile{sections/conclusion}

\clearpage
\printbibliography[heading=bibintoc]{}

\printglossary[type=\acronymtype]

\subfile{sections/appendix}

\end{document}