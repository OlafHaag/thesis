\documentclass[../main.tex]{subfiles}

\begin{document}

\section{Discussion}

% Inclusions and exclusions.
\subsection{Participants}
Some participants were able to perform the tasks very well while others performed poorly and had to be excluded from the analysis because of insufficient data.
Unfortunately, with this novel approach of ambulant data collection, one can not know for sure what the reasons for the difference in performance were.

One reason could have been a misunderstanding of the rules of the tasks, for example, the strict requirement of using the sliders concurrently.
Many trials were removed that did not meet this requirement.
Even when the sliders were grabbed at the same time, in a substantial amount of trials one slider was not moved beyond its initial position and these trials were subsequently removed by the outlier detection.
In the majority of cases this was true for the left slider (\gls{df1}).
About 90\% of the world's population is right-handed \parencite{PapadatouPastou2020}.
A dominant use of the right thumb could explain the majority of outliers being a result of not using the left slider while trying to fulfill the tasks (\cref{fig:Outliers}).
In conditions which only displayed the secondary constraint on one slider, the intended effect of requiring both \gls{dfs} to reach equal final states was implicit and not explicitly conveyed.
The goal of these tasks may not have been discovered by the participants and caused different control laws to be employed.

The instructional material was conveying the objective of the task but whether participants followed the instructions depended on them.
No instrumental variable was used to survey participants' willingness to fulfill the tasks as intended.

Two out of three people from the age group 60-69 years were excluded.
Dexterity in the hands decreases with age and older people take longer to achieve tasks \parencite{Hackel1992}.
\Textcite{Boatright1997} also reported a decrease in thumb strength starting at the age of 60 years.
However, a larger sample would be needed to assess whether age has a systematic influence on the performance of the tasks.
There are other factors about participants that are unknown, for example, neuromuscular disorders or traumas to participants' upper extremities, or impairments to the vision.
There's ample evidence for the importance of the cerebellum for fast trial-by-trial error-based learning that can be severely impairment in patients with cerebellar diseases \parencite{Golla2008}.
These issues can be better accounted for with preselection in a laboratory setting.

% Slider usage.
\subsection{Degrees of Freedom}
The coordination of elemental variables \gls{df1} and \gls{df2}, the left and right sliders in this study, across participants seemed similar.
Both sliders were moved by approximately the same amount and showed equal variance, even though the right slider showed slightly larger final states.
There was some exploration along the \gls{ucm}, but the means were near the center of the \gls{ucm}.
Maybe using both thumbs synchronously under a time constraint was more efficient, as the target could be reached quicker.
This would have been the shortest path to the \gls{ucm} from the starting position.
If only one thumb was used to mainly drive the radius of the disc, one may need to move faster to reach the target and sacrifice precision and feedback control.
Moving both thumbs allows slowing down a bit and receive more feedback for subsequent corrective action.
Nevertheless, there was considerable variability in the data, which either could mean participants explored the space, or the motor planning and output was very noisy under these conditions.
Of course, one important contributing factor to the similarity of the data across participants was that dissimilar outliers were removed before analysis.

Another reason for the concentration of mean values around the center could be the second training block in which the center was the desired solution.
Having that training block at the beginning could have primed the participants to choose similar solutions in the following task \parencite{Verstynen2011}.
This is a flaw in the design and the training for the treatment block should have been placed directly prior to the treatment block.
But the training block had only a few repetitions what makes this effect less likely.

Overall, \gls{df2} showed larger final state values than \gls{df1}.
The slider for \gls{df2} was placed on the right side of the display and it's assumed that the majority of participants were right-handed.
One way to interpret the difference is that the dominant hand was used to drive the sought after change more strongly.
Handedness is considered to be the preferred hand by an individual for tasks requiring dexterity, often called the dominant hand.
It is associated with better fine motor skills.
Larger mean values for \gls{df2} were also observed for tasks in which the implicit goal was for both \gls{dfs} to be equal.
Maybe error-based learning in the second block did not overcome use-dependent learning from the first block in which participants pushed slightly more on the right slider.

There was still considerable variability in the elemental variables' final states for tasks with a single solution (tasks \gls{df1}, \gls{df2}, and \gls{df1}|\gls{df2}).
A reduction in \gls{vucm} without an increase in \gls{vort} during these tasks would have been associated with a decrease in the variability of elemental variables.
A decrease of variability in elemental variables when a single solution was required in block 2 was not observed.
On the contrary, variability of \gls{dfs} that did not have an explicit additional task in block 2 even increased, even though the goal for both \gls{dfs} was implicitly the same.
This effect was not foreseen during the planning of the study.

Such an effect could occur due to differences in moment of the thumbs' movements, with one thumb moving faster than the other, which was possibly focussed on more.
Faster movements typically lead to increased variability of final states along the movement direction \parencite{Fitts1954}.
A difference in movement speed could occur, for example, if participants tried to execute tasks asynchronously one after the other such that the later \gls{df} had less time to finish.
But there was no discernible difference between onset and duration between \gls{dfs} in these tasks to indicate such a strategy.
The mean grabbing duration of the slider handles was around one and a half seconds.
Release of the slider handles mostly occurred at the end of the time limit.
Having a time and accuracy constraint could be a difficult task to perform for many people.
More insights could be gained if the data was not limited to the final states but the whole movement was recorded.

Another possible explanation could be that attention may have shifted to the slider that was provided with additional feedback.
It is unknown whether participants fixated on the widgets in the middle of the screen which visualized the task goals, or whether participants paid more attention to the sliders.
The additional instructions and presence of more visual elements on the screen during the second block could have caused split-attention effects that influenced performance \parencite{Chandler1991}.
This is a flaw in the design of the stimuli material which should have had similar elements in the other blocks but without the additional instructions and tasks.
Participants even could have been attending external stimuli in their surroundings that weren't part of the tasks.
As \textcite{Todorov2009} pointed out, the estimator may not only be concerned with elemental variables and the target that they interact with but also with a multitude of other things that have no relevance to the motor actions.

The task goals were not visualized directly on the slider tracks as final positions to reach, but were achieved by making the transfer from the positions of the slider handles to the properties of the widgets in the center of the screen.
The observed variability could reflect a difficulty to plan ahead where the thumbs should come to a halt.
Continuous updating would be expected by the controller via feedback in the execution stage.
It would require updating of the target during the movement.
Maybe this large uncertainty about the target position for the end effectors contributed to a large part of the observed variability and presence of outliers.
This uncertainty would be attributable to the localization stage of a goal-directed movement.
According to \textcite{Beers2004}, outputs of the localization stage, i.e. the target and current thumb position, get used by the planning stage as input, which then generates motor commands accordingly.
The localization of the correct target on the slider track was associated with a great deal of uncertainty.
Also, if stronger control signals were used in tasks with secondary constraints that require higher precision per \gls{df}, an increase in signal dependent noise may have caused more variability and counteracted attempts to reduce variability.
There was, however, no requirement to increase muscle force to fulfill secondary tasks simultaneously to the first task, making the need for increased control signals and larger amounts of \gls{sdn} as a result less likely.

The intended manipulation during the second block did work to some limited extent.
The best results regarding the intent were achieved in the condition with both sliders having feedback for the secondary tasks (\gls{df1}|\gls{df2}), as variability did not noticeably differ between elemental variables.
This manipulation could, however, decouple the motor control commands for the thumbs, since each slider could as well be controlled independently.
Solving both secondary tasks incidentally also solved the first task.
This is analogous to \textcite{Diedrichsen2007} study in which participants either controlled one cursor or two cursors.

\Cref{hyp:dfdists} was only partly supported.
\Gls{dfs} did not follow normal distributions, but they were bell shaped.
Mean values of both \gls{dfs} were not exactly equal but close.

\Cref{hyp:duration} did not receive any evidence.
The duration for using the sliders was not appreciably different when a secondary task was present.

% Task performance
\subsection{Task Performance}
Stability of task performance of the first task decreased with the introduction of a secondary task, while mean task performance stayed the same.
It is imaginable that participants would need more time to accurately fulfill two tasks compared to one task and that the time limit could cause a systematic failure to finish the tasks in time.
The mean release time of the sliders in all tasks was towards the end of the available time limit.
Yet there was no indication of a change in onset and duration of the slider movement for blocks with concurrent tasks.
When feedback is given symmetrical for both \gls{dfs}, the accuracy is on average higher for both task goals.
These observations suggest that the time limit alone was unlikely to account for the drop in performance stability.

An observation of the histograms of elemental variables \gls{df1} and \gls{df2} showed that for the various tasks different motor strategies were employed.
Since an increase in the variability of task performance in the treatment block was observed in all conditions, this increase may not be attributable to any particular motor strategy, but to the mere presence of an additional task and stimulus material.
Besides placing attention to new visual elements for secondary tasks, participants may have placed more importance on fulfilling the additional task than on the first task.
Both goals were assumed to be perceived as equally important, though there were no incentives to fulfill the tasks.
Rewards for good task performance may improve accuracy in the tasks through motivation.

Statistical analyses of the data did provide some evidence for \cref{hyp:performance}.
The goal of the first task, i.e. matching the disc size to the ring, was satisfied on average in all blocks, even when performance stability decreased.
An equivalence test for final states between \gls{dfs} provided some evidence that the second task goal was also met.
The experimental manipulation aimed to evoke a strategy of equivalent motor commands seems to have worked for most trials.
There is less difference between degrees of freedom for the secondary task when feedback is given on both \gls{dfs}.
Furthermore, this strategy seems to also have been employed in tasks that had no additional constraints by participants in the condition with feedback on both degrees of freedom.
Since blocks 1 and 3 aren't any different from the other conditions, this may hint to the sensitivity of the test of equivalence to the number of samples.
The test also expects normally distributed variables, which \gls{df1} and \gls{df2} deviated from.
The results of the test may, therefore, not be robust and meaningful.
Though there was support for \cref{hyp:performance}, the author of this study expected better performance by participants.

% PCA
\subsection{Principal Component Analysis}
The directions of the first principal components in the pre- and post-test were close to the direction of the \gls{ucm} but deviated by a few degrees.
The alignment disappeared during the manipulation in block 2.
The median interior angles between the principal components and the \gls{ucm} vector were comparable across treatment conditions.

It would have been practically difficult to compare the mean absolute directions and magnitudes of principal components across tasks because the components between participants were in some cases pointing in opposite directions.
Mean values of directions of components in the coordinate system of \gls{df1} and \gls{df2} would have been nonsensical and would have dissolved the orthogonal relationship between components.

The mean explained variance showed large differences between principal components.
The first component explained between 74\% and 85\% of the variance on average across all tasks, leaving the rest of the variance to be explained by the second component.
Since elemental variables in the pre- and post-test, as well as in the \gls{df1}|\gls{df2} task showed similar variances, the disparity in the magnitude of principal components for these tasks suggest the presence of a synergy.
In blocks where \gls{df1} and \gls{df2} received feedback for the secondary task, an increase in variability for the respective other \gls{df} was observed which is likely to contribute to the magnitude of the first principal components and its increased deviation from the \gls{ucm} vector.
The stronger deviation of principal components from the \gls{ucm} and the large proportion of explained variance by the first principal component during block 2 suggest that the point cloud was sheared or rotated compared to the pre- and post-tests.

The findings demonstrate the likeness between \gls{pca} and \gls{ucm} analysis.
The \gls{pca} can provide additional information about the actual direction of the greatest variance which \gls{ucm} analysis can not.

% Synergy
\subsection{Synergies}
The \cref{hyp:5} which is based on the \gls{ofct} did only receive little evidence by the analysis of variance components, since \gls{vort} did increase but \gls{vucm} did not decrease.
This effect provides evidence against \cref{hyp:6} which was based on \textcite{Zhang2008}'s claim, that any solution could simply be chosen without increasing \gls{vort} and that variance within the \gls{ucm} could be used to fulfill secondary tasks.

The variance of \gls{df1} and \gls{df2} distributions were supposed to be equal but were not in all conditions.
A change in the synergy index could be due to changes in variance difference between \gls{df1} and \gls{df2}.
If one of them gets larger, both \gls{vucm} and \gls{vort} increase, while the synergy index decreases.
That could mean the \gls{cns} is not stabilizing the performance variable of their linear combination but the value of an elemental variable.

Analyses like the Wilcoxon signed rank test and analysis of the synergy index do not provide enough information about the change within \gls{vucm} and \gls{vort}, only the relative change between them.
Different interaction effects like proposed by \cref{hyp:5} and \cref{hyp:6} cannot be evaluated using the synergy index alone.

The synergy index decreased more than would have been explained by changes in the magnitude of principal components.
The radius of the white disc might not be the performance variable that was being stabilized by the \gls{cns} motor commands.
It might have been something else like momentum or speed.
Or it might not have been the only variable that was being stabilized and simultaneous stabilization of different variables lead to trade-offs in stability.

The condition called \gls{df1}|\gls{df2} which had secondary tasks on both \gls{dfs} might be interpreted in the same way as the two-cursor task in the experiment by \textcite{Diedrichson2007}.
Then it was not actually controlled as one system with a secondary task constraint, but as two separate tasks for each hand.
Still, the manipulation of eliciting control signals of equal strength could have worked under this condition.

% Model Comparison
\subsection{Model Comparison}
There was no single best model that explained the data-generating process across participants.
This compares to a study by \textcite{Zhang2008} who reported having not found a default motor action pattern among participants.
\Cref{hyp:0}, as operationalized by \cref{model:0}, received only moderate support by a single participant.
Otherwise, there was little evidence to support the null model.
This agrees with the analysis of the synergy index.
\Cref{model:1} also delivered little support for \cref{hyp:1} which is in agreement with the observed change in the synergy index.
\Gls{vucm} and \gls{vort} were not constant throughout the blocks.
A few participants employed a motor strategy compatible with \cref{model:2} which supports \cref{hyp:2}.
For these participants synergies were either not present or too small to detect, while the secondary constraint increased the overall variability.
\Cref{hyp:3} that proposes both main effects of block and direction of projection overall received little evidence by \cref{model:3} but was supported strongly by data from a single participant.
\Cref{model:4} was the model with the highest average probability across participants, providing moderate evidence for \cref{hyp:4}, while it did not perfectly explain the data of any participant in particular like other model were able to.
This hints at a stronger increase in \gls{vort} than the amount by which \gls{vucm} was decreased.
With the second highest average probability among chosen models \cref{model:5} provides moderate evidence for \cref{hyp:5} which reflects predictions by the \gls{ofct}, put forward by \textcite{Todorov2004}.
This model fit very well for 4 participants.
No substantial evidence was provided by \cref{model:6} for \cref{hyp:6}.
Claims by \textcite{Zhang2008,Danion2011} that a reduction of \gls{vucm} was possible without increasing \gls{vort} did not apply to the task of this study.
For data from three participants \cref{model:7} was a good fit.
But overall, \cref{hyp:7} was not strongly supported by the data.
This is in agreement with findings by \textcite{Latash2003}, who reported a decline of \gls{vucm} only after the first 100 trials.
This study only had a total of 90 trials.
In a study by \textcite{Wu2012}, changes in variance components were reported after practice sessions that lasted 1.5 hours.
This study lasted about 15 minutes.
Use-dependent effects as a cause are therefore unlikely.

% HLM
%\subsection{Hierarchical Linear Model}
% ToDo: HLM

% Outlook
%\subsection{Outlook}
% ToDo: limited database capacity, record more data, record whole movement
% ToDo: Seems that Todorov chose different initial states (2D Gaussian). their final state was nearest solution on the UCM! This creates some of the variability along the UCM. I start with the same initial state every time. Todorov2005; Could be tested in a new design?

% ToDo: "the general effect of increased movement time is to improve performance: both bias and overall variability decrease, while the exploitation of redundancy increases." Todorov2002; vary time limitin a next study

% ToDo: If most of the results point to the extraneous load theory as being responsible for data, then the difference between blocks in unconstrained tasks and constrained tasks could be mitigated by including the colored arcs and colored slider tracks in the unconstrained tasks as well, but without any goal marker or instruction.

% ToDo: Couldn't record whole movement, because of restrictions in database size. Could otherwise take a look at corrections during movement.

% ToDo: Find relationship between end effector final states and actual joint configurations or muscle forces in laboratory.

% ToDo: Tracking task, learn tracking of radius trajectory (randomly shuffled half-sine waves). At a specific normalized time, one posture per trial is sampled. The analysis is then repeated at different time slices. In general, this approach might raise concerns in dynamic tasks which are not appropriately timed. When a task is self-paced, there is no guarantee that events occurring in different trials at the same normalized time are necessarily related. In any case, the method would still be applicable to tracking tasks where, repeatedly across trials, a subject is asked to track a visible target which evolves along a predefined path at a predefined speed (the whole path might or might not be displayed). At any given time, the subject is supposed to be on target. Therefore, postures at that very time could be sampled across trials and used in the UCM analysis, as for a static task.    Widjaja, F., Xu, H., Ang, W. T., Burdet, E., & Campolo, D. (2013). Analysis of accuracy in pointing with redundant hand-held tools: a geometric approach to the uncontrolled manifold method.

% ToDo: It would be interesting to see a comparison of the trajectories between the "2-dimensional point mass" and the 2 "one-dimensional point masses", i.e. how the trajectory characteristics differ between the 2 tasks.

% ToDo: Studies with anticipatory synergy adjustment (ASA), change in goal during task, e.g. change radius of circle. would destabilize task performance in anticipation of change. Would mean thumbs would move beforehand? The emergence and disappearance of multi-digit synergies during force-production tasks Shim JK, Olafsdottir H, Zatsiorsky VM, Latash ML Exp Brain Res. 2005 Jul; 164(2):260-70.
% a negative synergy has been observed when a quick change of the performance variable is required

% ToDo: virtual third arm movement control by steering along the UCM


%Discuss results, taking into account the mechanism by which the experimental manipulation was intended to work (causal pathways) or alternative mechanisms.
%Discuss the success of, and barriers to, implementing the experimental manipulation; fidelity of implementation if an experimental manipulation is involved.
%Discuss generalizability (external validity and construct validity) of the findings, taking into account
%‒ characteristics of the experimental manipulation
%‒ how and what outcomes were measured
%‒ length of follow-up
%‒ incentives
%‒ compliance rates
%Describe the theoretical or practical significance of outcomes and the basis for these interpretations
%
%% Support of Original Hypotheses
%Provide a statement of support or nonsupport for all hypotheses, whether primary or secondary, including
%‒ distinction by primary and secondary hypotheses
%‒ discussion of the implications of exploratory analyses in terms of both substantive findings and error rates that may be uncontrolled
%
%% Similarity of Results
%Discuss similarities and differences between reported results and work of others.
%
%% Interpretation
%Provide an interpretation of the results, taking into account
%‒ sources of potential bias and threats to internal and statistical validity
%‒ imprecision of measurement protocols
%‒ overall number of tests or overlap among tests
%‒ adequacy of sample sizes and sampling validity
%
%% Generalizability
%Discuss generalizability (external validity) of the findings, taking into account
%‒ target population (sampling validity)
%‒ other contextual issues (setting, measurement, time; ecological validity)

% Implications
%\subsection{Outlook}
%Discuss implications for future research, program, or policy.

\end{document}