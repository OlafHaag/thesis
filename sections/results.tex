\documentclass[../main.tex]{subfiles}

% New commands for formatting results. Optional precision.
\newcommand*{\p}[2][4]{\emph{p}=\num[round-mode=places,round-precision=#1]{#2}} 
% Can't use the sampleinfo in methods here (local to methods), so declare it again.
\newcommand{\sampleinfo}[1]{\DTLfetch{sample}{key}{#1}{value}}
% New commands for formatting numbers. Optional precision.
\newcommand*{\dbnum}[2][4]{\num[round-mode=places,round-precision=#1]{#2}} 

\begin{document}

\section{Results}

\subsection{Inclusion and Exclusion}
\DTLgetvalueforkey{\contamination}{value}{sample}{key}{contamination}

There were a total of \sampleinfo{n_invalid_trials} trials that were not performed according to the instructions and that were removed.
This included trials in which one or both sliders were not used at all or were not used concurrently.
The contamination rate to identify outliers was manually tweaked to \percentage{\contamination}.
As a result, an additional \sampleinfo{n_trials_outliers} trials were identified as outliers and excluded from further analysis, see \cref{fig:Outliers}.

\begin{figure}[!h]
    \begin{center}
        \caption{Scatter-plot of data for \glsxtrshort{df1} and \glsxtrshort{df2} with outlier threshold.}
        \includegraphics[width=\textwidth,trim=2]{scatter-outliers.pdf}
        \label{fig:Outliers}
    \end{center}
    \figurenote{%
        The black line represents the solution space to the first task present in all blocks.
        The red cross shows the solution to the concurrent tasks where there was feedback either on the \gls{df1}, \gls{df2}, or both (\gls{df1}|\gls{df2}).
        The gray ellipsoid is the outlier threshold.
        Data points outside the threshold were considered outliers and were removed before further analysis.%
    }
\end{figure}

To ensure there weren't other systematic influences on outlier detection, the duration and release of the slider grabs were inspected between outliers and non-outliers, see \cref{fig:OutlierDuration} and \cref{fig:OutlierRelease}.
Detailed statistics are listed in \cref{app:Outliers}.
The mean duration of sliders being grabbed was slightly lower for trials deemed as outliers than for non-outliers but on average by less than 0.4 seconds.
The standard errors of the mean values were a magnitude larger for outlier trials.
No practically relevant differences between outliers and non-outliers were found for the release of the sliders.

\begin{figure}[!h]
    \caption{Comparison of grab duration for outliers and non-outliers per \gls{df}.}
    \begin{center}
        \subfloat[\Gls{df1} duration by outlier.]{%
            \includegraphics[width=\textwidth]{boxplot-df1_duration_by_outlier.pdf}%
            \label{fig:df1OutlierDuration}%
        }

        \subfloat[\Gls{df2} duration by outlier.]{%
            \includegraphics[width=\textwidth]{boxplot-df2_duration_by_outlier.pdf}%
            \label{fig:df2OutlierDuration}%
        }
    \end{center}
    %\figurenote{  }
    \label{fig:OutlierDuration}%
\end{figure}

\begin{figure}[!h]
    \caption{Comparison of grab release time for outliers and non-outliers per \gls{df}}
    \begin{center}
        \subfloat[\Gls{df1} release by outlier.]{%
            \includegraphics[width=\textwidth]{boxplot-df1_release_by_outlier.pdf}%
            \label{fig:df1OutlierRelease}%
        }

        \subfloat[\Gls{df2} release by outlier.]{%
            \includegraphics[width=\textwidth]{boxplot-df2_release_by_outlier.pdf}%
            \label{fig:df2OutlierRelease}%
        }
    \end{center}
    %\figurenote{  }
    \label{fig:OutlierRelease}%
\end{figure}

After exclusion of the outliers, \sampleinfo{n_sessions_excluded_by_trials_count} participants were excluded from further analysis, because they did not meet the inclusion criteria, see \cref{fig:Exclusions}.
If a participant didn't perform at least 60\% of trials in a session sufficiently well, that participant was removed from further analyses.

Using the interactive graphs of the study's supplementary material (Jupyter notebooks), further exploration was conducted to assert the possible influence of gaming experience and difficulty ratings of a participant.
It appears mobile gaming experience did not have a systematic influence on the inclusion criteria.
In the few cases where a task was rated very difficult by participants, there was one individual who didn't reach the inclusion criteria.
Other difficulty ratings didn't seem to coincide with the exclusion of the data.
Two out of three participants in the group of 60 to 69-year-olds didn't reach the inclusion criteria.

\clearpage
\subsection{Descriptive Statistics of Degrees of Freedom}
\subsubsection{Analysis of Slider Usage}
The distributions of the onset of grabbing the slider handles for each task appear to be bell-shaped with long tails in the direction of progressing time, as visible in \cref{fig:GrabOnset}.
There are no discernible differences between different conditions, as \cref{tab:Onset} reveals.

\begin{figure}[!h]
    \caption{Time measures for sliders \gls{df1} and \gls{df2}.}
    \begin{center}
        \subfloat[Onset of \gls{df1} and \gls{df2} being grabbed.]{%
            \includegraphics[width=\textwidth]{violin-dof_onset.pdf}%
            \label{fig:GrabOnset}%
        }

        \subfloat[Duration of \gls{df1} and \gls{df2} being grabbed.]{%
            \includegraphics[width=\textwidth]{violin-dof_duration.pdf}%
            \label{fig:GrabDuration}%
        }
    \end{center}
    %\figurenote{%
    %  \protect\subref{fig:GrabOnset})
    %  \protect\subref{fig:GrabDuration})
    %}
    \label{fig:DoFTimes}%
\end{figure}

\subsubsection{Data Distribution}
The visual inspection of the QQ-plots (\cref{fig:QQDoF}) show deviations from normal distributions for the final states of \gls{df1} and \gls{df2}, especially in the lower and higher quantiles. The best-fit lines follow the 45\si{\degree} angle.

\begin{figure}[!h]
    \begin{center}
        \caption{QQ-Plots of \glsxtrshort{df1} and \glsxtrshort{df2} by task.}
        \includegraphics[width=\textwidth]{qq-plot-dof-grid.pdf}
        \label{fig:QQDoF}
    \end{center}
    \figurenote{%
        The red solid line shows the best-fit line (linear regression) for the data.
        Dashed red lines show 95\% point-wise confidence envelope.
        Most distributions show deviance from the normal distribution in the tails.
    }
\end{figure}

No visually discernible difference exists between the pre- and post-test distributions of \gls{df1} and \gls{df2} final states.
Very notable is the presence of clear differences between the tasks in the treatment block, even though the solutions to these tasks are all identical.
An overall tendency can be seen for higher mean values of \gls{df2} over \gls{df1}.

\begin{figure}[!h]
    \begin{center}
        \caption{Final states of \gls{dfs}.}
        \includegraphics[width=\textwidth]{barplot-dof_mean.pdf}
        \label{fig:BarDoFMean}
    \end{center}
    \figurenote{%
        Bars show the mean value of the mean final state per participant.
        Vertical bars represent mean standard deviations of final states, i.e. averaged across standard deviations per participant.
    }
\end{figure}

\begin{samepage}

    \begin{figure}[!h]
        \begin{center}
            \caption{Histogram of pre-test data.}
            \includegraphics[width=\textwidth]{histogram-pre.pdf}
            \label{fig:HistDoFTaskPre}
        \end{center}
    \end{figure}

    \begin{figure}[!h]
        \begin{center}
            \caption{Histogram of task \gls{df1}.}
            \includegraphics[width=\textwidth]{histogram-df1.pdf}
            \label{fig:HistDoFTaskDf1}
        \end{center}
    \end{figure}

    \begin{figure}[!h]
        \begin{center}
            \caption{Histogram of task \gls{df2}.}
            \includegraphics[width=\textwidth]{histogram-df2.pdf}
            \label{fig:HistDoFTaskDf2}
        \end{center}
    \end{figure}

    \begin{figure}[!h]
        \begin{center}
            \caption{Histogram of task \gls{df1}|\gls{df2}.}
            \includegraphics[width=\textwidth]{histogram-df1-df2.pdf}
            \label{fig:HistDoFTaskDf12}
        \end{center}
    \end{figure}

    \begin{figure}[!h]
        \begin{center}
            \caption{Histogram of post-test data.}
            \includegraphics[width=\textwidth]{histogram-post.pdf}
            \label{fig:HistDoFTaskPost}
        \end{center}
    \end{figure}
\end{samepage}

\subsubsection{Manipulation Check}
A look at the mean values of final states (\cref{fig:BarDoFMean}, \cref{tab:FinalStates}, and \cref{fig:DoFMean}) confirms observations already made in the histograms but draws a more concise picture.
The right slider (\gls{df1}) was pushed further on average than the left slider (\gls{df2}).
Variability of elemental variables in block 2 was increased for sliders that were not connected to a secondary visual target.
The mean values in \cref{fig:BarDoFMean} and \cref{tab:FinalStates} were first computed per participant and then averaged, whereas the mean values in \cref{fig:DoFMean} were calculated across participants.

\begin{figure}
    \begin{center}
        \caption{Mean final state per \gls{df} across participants.}
        \includegraphics[width=\textwidth]{line-plot-dof_mean.pdf}
        \label{fig:DoFMean}
    \end{center}
    \figurenote{%
        Mean values were computed across participants.
        Vertical bars represent the 95\% confidence interval.
        The target value in block 2 for both \gls{df} was 62.5\%.
    }
\end{figure}

\clearpage
\subsection{Task Performance}
\subsubsection{Task Performance I: Sum of Degrees of Freedom}
The first performance variable to be analyzed is the diameter of the white disc, which depended on the sum of both normalized \gls{dfs} in percent.
The target was 125\%.

Participants differ in how they perceived the difficulty of the tasks.
Some individuals perceived the different tasks across the experimental blocks as equally difficult.
But others rated the second block, in which concurrent tasks had to be performed, as more difficult than the blocks with only one task.
When performing the third block, participants gave similar ratings as for the first block.
\Cref{fig:LineRatings} shows ratings from all included participants.

\begin{figure}
    \begin{center}
        \caption{Difficulty ratings by block.}
        \includegraphics[width=\textwidth]{line-plot-rating_block.pdf}
        \label{fig:LineRatings}
    \end{center}
\end{figure}
% ToDo: Mean difficulty rating by task.

\DTLloaddb{levene_sumvar_rating}{res/reports/sumvar_rating_levene.csv}
\DTLassign{levene_sumvar_rating}{1}{\LeveneW=W,\LeveneP=pval}

Does difficulty rating correlate with performance?
\Cref{fig:ScatterRatingsVariance} reveals that the variance of the performance variable increases on average with the difficulty rating.
The variance in the residuals increases with the rating.
Equidistance between the ratings is not guaranteed.
Using a simple linear regression model to analyze this heteroscedastic data would, therefore, not be appropriate.
An assessment of the homoscedasticity using Levene's test revealed that the variances for the performance variable (sum variance) grouped by rating are not equal, $W$=\dbnum[2]{\LeveneW}, \p[3]{\LeveneP}. Strictly speaking, the assumption of independent samples was not fully met, but the result is not surprising given what can be observed in \Cref{fig:ScatterRatingsVariance}.

\begin{figure}
    \begin{center}
        \caption{Correlation of difficulty rating with task performance.}
        \includegraphics[width=\textwidth]{scatter-rating_variance.pdf}
        \label{fig:ScatterRatingsVariance}
    \end{center}
    %\figurenote{%
    %}
\end{figure}

As can be seen in \cref{fig:SumMean}, the final state mean values for the pre- and post-test are more spread out along the solution space across participants.
On visual inspection, the treatment tasks in block 2 seem to be less precise on average, as there are more mean values further away in the task-relevant direction from the solution than for the single-task blocks.

\begin{figure}
    \begin{center}
        \caption{Mean final states by task.}
        \includegraphics[width=0.75\textwidth]{scatter-sum_mean.pdf}
        \label{fig:SumMean}
    \end{center}
    \figurenote{%
        Each point shows the mean of one participant for the respective task.
        The black line represents the solution space to the first task present in all blocks.
        The gray cross shows the solution to the concurrent tasks where there was feedback either on \gls{df1}, \gls{df2}, or both (\gls{df1}|\gls{df2}).
    }
\end{figure}

The normality of the distribution of the sum of degrees of freedom was again visually assessed with QQ-plots (\cref{fig:QQSum}).
The best-fit lines follow the 45\si{\degree} angle, but the data is a bit S-shaped, indicating that there is more deviation in the tails from the normal distribution.

\begin{figure}
    \begin{center}
        \caption{QQ-Plots of performance variable by task.}
        \includegraphics[center=2.0\linewidth,width=\linewidth]{qq-plot-sum_ci.pdf}
        \label{fig:QQSum}
    \end{center}
    \figurenote{%
        The red solid line shows the best-fit line (linear regression) for the data.
        Dashed red lines show 95\% point-wise confidence envelope.
        The pre- and post-test contain more data since each participant did them.
        The distributions show a slight S-curve.
        For the pre- and post-test there is also deviance from a normal distribution in the tails.
    }
\end{figure}

The sum of \gls{dfs} closely matched the target on average (\cref{fig:BarSum}).
The standard deviation appears larger for blocks with concurrent tasks.
The lower variance for the distribution of the sum in pre- and post-tests suggests a greater stability in the performance variable when no additional task is present.
Since the pre- and post-test data encompasses all participants, there are more samples in these tasks.
To assess whether this effect appears due to sample size, the data was grouped by condition (\cref{fig:BarSumCondition}).
When looking at the conditions individually, a consistent pattern of larger variability in the treatment block for the sum of both degrees of freedom can still be seen.
This suggests a destabilization of the performance variable when an additional task is introduced.
\Cref{tab:SumCond} shows the detailed values for the mean and standard deviation of the \gls{dfs} sum and confirms the visual impression. The standard deviation for the treatment blocks ranges from 6.15 to 8.18, while the standard deviation for blocks with a single task ranges from 2.22 to 4.01.
When individual contributions of participants are explored, it shows that there were big differences in the variability of task performance between participants, although on average they achieved to be in the vicinity of the target (\cref{fig:BarSumUser}).

\begin{figure}
    \begin{center}
        \caption{Bar-plot of performance variable by task.}
        \includegraphics[]{barplot-sum.pdf}
        \label{fig:BarSum}
    \end{center}
    \figurenote{%
        Vertical bars represent the standard deviations.
        The target value for this performance variable was 125 in all tasks.
    }
\end{figure}

\begin{figure}
    \begin{center}
        \caption{Bar-plot of performance variable by task.}
        \includegraphics[width=\textwidth]{barplot-sum_by_condition.pdf}
        \label{fig:BarSumCondition}
    \end{center}
    \figurenote{%
        Vertical bars represent the standard deviations.
        The target value for this performance variable was 125 in all tasks.
    }
\end{figure}

\begin{figure}
    \begin{center}
        \caption{Bar-plot of \gls{df} sum mean and variance by task and user.}
        \includegraphics[width=\textwidth]{barplot-sum_by_user.pdf}
        \label{fig:BarSumUser}
    \end{center}
    \figurenote{%
        Vertical bars show statistics per participant.
        The horizontal bars show the average statistics.
        A) Variance around the mean sum of degrees of freedom.
        B) The target value for this performance variable was 125\% in all tasks.
    }
\end{figure}

\clearpage
\subsubsection{Task Performance II: Mean Final State Difference}

The difference in mean values of degrees of freedom is the performance variable for the additional task, since the solution to the treatment tasks was that both degrees of freedom were at 62.5\%.
The distributions of the difference between each \gls{df} and the additional target reveal again an influence of condition on behavior (\cref{fig:HistTargetDiff}).
In the task with both \gls{dfs} connected to arcs the distributions have a strong overlap.
For the tasks with a single arc, the \gls{df} that controlled the arc was, on average, closer to the target.
\Cref{tab:DiffTarget} reflects the trend that \gls{df2} was on average above the target while \gls{df1} was on average below the target, as was already suggested by \cref{fig:DoFMean}.
Noteworthy is, that also in tasks that did not require the \gls{dfs} to be at 62.5\%, they were quite close to that value.


\begin{figure}
    \begin{center}
        \caption{Distribution of difference from target value per \gls{df}.}
        \includegraphics[width=\textwidth]{histogram-target_diff.pdf}
        \label{fig:HistTargetDiff}
    \end{center}
    \figurenote{%
        Only data from block 2 is shown as this were the only blocks the difference to the target value mattered.
    }
\end{figure}

The intent to treat was that both degrees of freedom are equal. This may be achieved without reaching the intended target value precisely.
Instead of the difference to a target value, \cref{fig:BoxDiffDoF} shows the absolute difference between the \gls{dfs} themselves.
\Cref{tab:DiffDoF} shows detailed descriptive statistics about the difference.
Equivalence tests were conducted, set to a bound of a 5\% of the slider range as an acceptable error margin for the difference between degrees of freedom (\cref{tab:TOST}).
The alpha threshold to consider for evaluating \emph{p}-values after applying a conservative Bonferroni correction to control for error rates due to multiple testing is 0.0055.
The \emph{p}-values for the second blocks in conditions \gls{df2} and \gls{df1}|\gls{df2} fall below this threshold, \p{0.0003} \& \p{0.0000} respectively.
For block 2 of condition \gls{df1}, \p{0.0102}.

\begin{figure}
    \begin{center}
        \caption{Distribution of difference from target value per condition and block.}
        \includegraphics[width=\textwidth]{boxplot-dof_diff.pdf}
        \label{fig:BoxDiffDoF}
    \end{center}
    %\figurenote{}
\end{figure}

\clearpage
\subsection{Principal Component Analysis}
For blocks 1 and 3 principal components are expected to align with the \gls{ucm} vector and its orthogonal.
As was expected, each participant favored a different part of the solution space (\cref{fig:SumMean}).
The \gls{pca} was therefore applied for each participant separately.
Otherwise, components would only reflect variance across participants, but not within participants.
The mean explained variance and standard deviations are shown in \cref{tab:PCA}.
\Cref{fig:BarPCA} is an illustration of the same data.
Regardless of task, the first principal component explained between $74.36\%\pm11.24\%$ (df1|df2) and $84.34\%\pm12.74$ (post-test) of the variance.
This is surprising, since that means that there is still considerable co-variance in the data, where it was not expected, namely in tasks df1, df2, and df1|df2.

\begin{figure}
    \begin{center}
        \caption{Explained variance by principal component and task.}
        \includegraphics[width=\textwidth]{barplot-pca.pdf}
        \label{fig:BarPCA}
    \end{center}
    \figurenote{%
        Vertical bars represent standard deviations.
        Bars show the mean explained variance across participants.
    }
\end{figure}

The differences between the directions of the principal components and the vectors parallel and orthogonal to the \gls{ucm} were computed in degrees (\si{\degree}).
\Cref{tab:PCAAngle} shows that for the pre- and post-test the principal components align approximately with the \gls{ucm} (pre: 7.52\si{\degree}, post: 4.95\si{\degree}).
In the other tasks, for which either only one \gls{df} of both were visually connected to an arc, the principal components show large deviations from the \gls{ucm} (\cref{fig:HistPCAAngles}).
For tasks with only one \gls{df} providing a target for the participants, this can be attributed to the fact that participants in these conditions reduced variability only in the degree of freedom that was perceived as more important.
\input{res/reports/pca-ucm-angles.tex}

It's notable that the principal components in these two tasks seem to be mirrored across the UCM.

The distributions of interior angle across participants show that for the pre- and post-test the angles show mostly low values and are spread out for the treatment tasks.
\begin{figure}
    \begin{center}
        \caption{Distribution of interior angle between PC1 and \gls{ucm}.}
        \includegraphics[width=\textwidth]{histogram-PC1_UCM_angles.pdf}
        \label{fig:HistPCAAngles}
    \end{center}
    \figurenote{The bin size is 5.0\si{\degree}.}
\end{figure}

\clearpage
\subsection{Analysis of Synergies}
When inspecting the variance components parallel and orthogonal to the \gls{ucm}, it seems \gls{vucm} tends to be a bit larger than \gls{vort} (\cref{fig:LineProjectionVariance} \& \cref{fig:ViolinProjectionVariance}).
This suggests the presence of synergies with regard to the performance variable.
It also shows that both components tend to increase during the tasks with secondary constraints.

\begin{figure}
    \begin{center}
        \caption{\gls{vucm} and \gls{vort} by condition and block.}
        \includegraphics[width=\textwidth]{line-plot-projections.pdf}
        \label{fig:LineProjectionVariance}
    \end{center}
    \figurenote{%
        \Gls{vucm} is represented as parallel projection (green), \gls{vort} by the orthogonal projection direction (red).
        Vertical bars represent the 95\% confidence interval.}
\end{figure}

\begin{figure}
    \begin{center}
        \caption{\gls{vucm} and \gls{vort} by task.}
        \includegraphics[width=\textwidth]{violin-projections.pdf}
        \label{fig:ViolinProjectionVariance}
    \end{center}
    \figurenote{This figure illustrates that there are only a few extreme data points in blocks 1 and 3 of condition df1 responsible for the large standard deviations of \gls{vucm} in these blocks.}
\end{figure}


\DTLloaddb{wilcox}{res/reports/wilcoxon-projection-results.csv}
\begin{table}
    \caption{Wilcoxon Signed Rank Test for Projection Direction} % Title
    \label{tab:Wilcox}

    \sisetup{round-mode=places,
        table-number-alignment = center-decimal-marker,
        scientific-notation = fixed, fixed-exponent = 0
    }

    \begin{tabular}{
            l
            l
            S[table-format = 4.1, round-precision=1]
            S[table-format = 1.6, round-precision=4]
            @{}l}
        \toprule
        \bfseries Condition & \bfseries Task & \bfseries W & \bfseries \emph{p}   \\
        \midrule
        \DTLforeach*{wilcox}%
        {\cond=condition, \task=task, \teststat=test statistic, \pval=p}%
        {\DTLiffirstrow{}{\tabularnewline}%
        \cond               & \task          & \teststat   & \pval              &
        }
        \\\bottomrule
    \end{tabular}
    %\begin{tablenotes}[para,flushleft]
    %    {\small
    %        \textit{Note.}
    %    }
    %\end{tablenotes}
\end{table}

\DTLgetvalueforkey{\pval}{p}{wilcox}{condition}{overall}
%\DTLgetvalueforkey{\datavalue}{HeadB}{data}{HeadA}{Two}
The test statistic $W$ of the overall Wilcoxon signed-ranked test of the different projection components parallel and orthogonal to the \gls{ucm} is \DTLfetch{wilcox}{condition}{overall}{test statistic}, \p[3]{\pval} (see \cref{tab:Wilcox}).
To account for multiple tests the $\alpha$-level was Bonferroni corrected to 0.005.
The overall comparison indicates that the parallel projection scores (\gls{vucm}) were higher than the orthogonal projection scores (\gls{vort}) across conditions and participants.
Out of the tests of the individual blocks per condition across participants, only the post-test in condition \gls{df1}|\gls{df2} falls below the conservatively adjusted $\alpha$-level, \p{0.0002}.

Using a Bayesian RM ANOVA \parencite{Rouder2017} of the z-transformed synergy index $\delta V_z$ (specifying a multivariate Cauchy prior on the effects), the Bayes factor indicates that the data are 45061 times more likely under the model that includes block as the predictor, compared to the null model.
The null model is 3.968 more likely than a main effect model that only includes condition.
Utilizing the transitive property of Bayes factors, the Bayes factor ANOVA with default prior scales revealed that the main effect model that only incorporates block as a predictor was preferred to the main effects model that also includes condition by a Bayes factor of 3.31, and was preferred against the interaction model by a Bayes factor of 5.738.
The data provide moderate evidence for the model with block as a predictor.
\Cref{fig:QQSynergyIndexResiduals} shows the QQ-plot of the residuals of the synergy index to check the normality assumption for the residuals when conducting an ANOVA.
The residuals show slight deviations from normality in the lower quantiles, but they are still within the 95\% credible interval.

Post hoc comparisons of block 1 vs. block 2 and block 2 vs. block 3 revealed posterior  odds of 32.369 and 6205.352 against the null hypothesis, which indicates very strong and decisive evidence in favor of the alternative hypothesis with block as a predictor (\cref{tab:PostHocComparisons-Block}).
When comparing block 1 and 3, there was anecdotal evidence in favor of the null hypothesis, which was to be expected.
Detailed results of the repeated measures ANOVA are shown in \cref{tab:RMANOVA}.
On average the effects of block and condition explain 39.5\% of the observed variance, see \cref{tab:AOVRsquared}.
Descriptive statistics (\cref{tab:SynergyDescriptives}) and model averaged posteriors (\cref{tab:AOVModelAveragedPosteriorSummary}) show a decline for the synergy index in block 2, as seen in \cref{fig:LineSynergyIndex} and \cref{fig:AOVBlockDensity}, which resulted in weak synergies with regard to the first performance variable.

\input{res/reports/aov-dVz.tex}

\begin{figure}[!h]
    \begin{center}
        \caption{QQ-Plot of residuals for the synergy index.}
        \includegraphics[scale=0.5]{qq-plot-synergy_index_residuals.pdf}
        \label{fig:QQSynergyIndexResiduals}
    \end{center}
    \figurenote{%
        The Synergy Index was Fisher-z-transformed for analysis.
        Vertical bars represent 95\% credible intervals.
    }
\end{figure}

\begin{figure}[!h]
    \begin{center}
        \caption{Synergy Index by condition and block.}
        \includegraphics[scale=0.5]{line-plot-dVz_bw.pdf}
        \label{fig:LineSynergyIndex}
    \end{center}
    \figurenote{The Synergy Index was Fisher-z-transformed for analysis.}
\end{figure}

\begin{figure}[!h]
    \begin{center}
        \caption{Model-averaged posterior distributions for factor block.}
        \includegraphics[scale=0.5]{density-aov_block_posterior.pdf}
        \label{fig:AOVBlockDensity}
    \end{center}
    \figurenote{%
        This figure shows the mean differences and 95\% credible intervals for each of the factor levels of block normalized to the intercept (mean value of all the data).
    }
\end{figure}

\clearpage
\subsection{Bayesian Model Comparison}
The result of the Bayesian model-comparison of \crefrange{model:0}{model:7} did not reveal a dominant model across participants, see \cref{fig:ModelPosteriors} \& \cref{fig:HistModelPosteriors}.
The average posterior probability for \cref{model:0} was $\overline{P}(M0|D) = 0.04$, and $\max P(M0|D) = 0.69$ for a single participant.
The average posterior probability for \cref{model:1} was $\overline{P}(M1|D) = 0.02$, the highest $\max P(M1|D) = 0.19$ for any participant.
The average posterior probability for \cref{model:2} is $\overline{P}(M2|D) = 0.18$, with $\max P(M2|D) = 0.98$.
The average posterior probability for \cref{model:3} was $\overline{P}(M3|D) = 0.14$, and $\max P(M3|D) = 1.00$.
The average posterior probability for \cref{model:4} was $\overline{P}(M4|D) = 0.24$, the highest probability for any participant was $\max P(M4|D) = 0.80$.
The average posterior probability for \cref{model:5} was $\overline{P}(M5|D) = 0.20$, while $\max P(M5|D) = 1.00$.
The average posterior probability for \cref{model:6} was $\overline{P}(M6|D) = 0.04$, with $\max P(M6|D) = 0.71$.
The average posterior probability for \cref{model:7} was $\overline{P}(M7|D) = 0.14$, with the highest value for this model at $\max P(M7|D) = 1.00$.

\begin{figure}
    \begin{center}
        \caption{Bayesian Model Comparison Posteriors.}
        \includegraphics[height=0.7\textheight]{heatmap-posteriors.pdf}
        \label{fig:ModelPosteriors}
    \end{center}
    \figurenote{%
        Models M0-M7 correspond to \crefrange{model:0}{model:7} which were constructed to reflect \crefrange{hyp:0}{hyp:7}.
        The distribution of prior probabilities for the models was uniform.
    }
\end{figure}

\begin{figure}
    \begin{center}
        \caption{Histogram of Model Posterior distribution.}
        \includegraphics[width=\textwidth]{histogram-posteriors.pdf}
        \label{fig:HistModelPosteriors}
    \end{center}
    \figurenote{%
        Models represent assumptions made by postulated \crefrange{hyp:0}{hyp:7} about the data-generating processes.
    }
\end{figure}

%\clearpage
%\subsection{Hierarchical Linear Model}
%\includepdf[pages=-]{res/reports/MixedModel.pdf}

%% BAYESIAN ANALYSES
%
%% Model
%Completely specify both the systematic and the stochastic parts of the analyzed model, and give the %rationale for choices of functional forms and distributions.
%
%%Distributions
%Describe the prior distribution(s) for model parameters of interest. If the priors are informative, %state the rationale for that choice, and conduct a sensitivity analysis to check the dependence of the %results on the prior distribution.
%Describe the posterior distribution(s) for substantive model parameters and important functions of the %parameters.
%If feasible, report the highest posterior density (HPD) interval for each parameter or function.
%Plot or describe the joint distribution if substantive parameters are correlated.
%If predictions are made for observable quantities, make available either the actual predictive %distribution and parameter estimates, report summary statistics that describe the distribution, or %provide a graphical summary.
%
%% Likelihood
%Describe the unnormalized or normalized likelihood if the prior distribution is informative.
%
%% Plots
%Include the prior distribution, likelihood, and posterior distribution in a single plot (i.e., a %triplot) if the prior distribution is informative and plots are to be presented.
%
%% Decisions
%Report the utilities, or costs and benefits, and explain how they were derived if the data are used for %decision making about possible actions.
%Also provide a sensitivity analysis for various prior distributions or assumptions about utilities for %the decision.
%
%% Computations
%Describe in detail, including the number of chains, the number of burn-in iterations for each chain and %thinning if Markov chain Monte Carlo (MCMC) or another sampling procedure is used.
%Specify the methods used to check for convergence and their results.
%
%% Model Fit
%Describe the procedures used to check the fit of the model and the results of those checks.
%
%% Bayes Factors
%Specify the models being compared if Bayes Factors are calculated.
%‒ Report the Bayes Factors and how they were interpreted.
%‒ Test the sensitivity of the Bayes Factors to assumptions about prior distributions.
%
%% Bayesian Model Averaging
%State the parameter or function of parameters being estimated in Bayesian model averaging.
%Either plot the distribution or list the mean and standard deviation if it is near normal; otherwise, %list a number of percentiles for the distribution if it is not near normal.
%Describe how the models were generated and, if a reduced set was used for averaging, how the selection %was made and which models were used in the averaging.

\end{document}