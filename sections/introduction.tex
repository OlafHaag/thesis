\documentclass[../main.tex]{subfiles}

\begin{document}

% Problem
\section{Introduction}
% State the importance of the problem, including theoretical or practical implications.
Movement of the body has a pivotal role in most human experiences.
The literature often distinguishes two types of movement, namely reflexes and volitional movements.
In contrast to reflexes, which are a reaction to a sensation, volitional movement is the execution of an action by intent \parencite{Schwartz2016}.
The purpose of movement is not only to achieve a certain motor task, but also to actively sense our environment efficiently \parencite{Yang2018a}.
For example, the report mentions that we orient our eyes to points of interest within the visual scene. We could also move our whole body to get a better look at a partly occluded object.
\Textcite[p. 202]{Llinas2002} even argues that the ultimate reason for the evolution of the \gls{cns} is to allow active movements and predict their outcomes on the basis of sensory information.
Thus, he claims the most likely central function of the brain is to make predictions.
According to \citeauthor{Llinas2002}, movement and mind\footnote{Llinás prefers \textquote{mindness}.} are deeply related (p. 5).
Despite the apparent importance of movement to our experiences, motor control is a relatively young research field.
It explores how the \gls{cns} achieves purposeful, coordinated movements and postures of body parts and how they interact with each other, as well as with the environment \parencite{Latash2010b}.

The human musculoskeletal system has more degrees of freedom (e.g. joints, muscles) than necessary to accomplish most everyday tasks and is in that sense a redundant system \parencite{bernstein1967}.
The human arm, for example, has 3 joints (shoulder, elbow, wrist) and 7 \gls{dfs}, driven by 18 muscles \parencite{Schuenke2000}.
At each joint there are more muscles acting upon it than kinematic degrees of freedom of that joint.
The term redundancy in this context is two-fold: There are many ways in which the \gls{dfs} can be configured to achieve any one particular pose, and secondly, there exists an infinite number of possible spatial or temporal trajectories (of joint rotations or muscle activation patterns) to reach a certain goal \parencite{Solnik2013, Latash2002}.

This leads to a flexibility that allows to fulfill concurrent tasks with a multi-joint limb by configuring the involved \gls{dfs} in directions that do accomplish each task at the same time.
Imagine you are leaving your kitchen with a mug of coffee in one hand and a plate of cake in the other and you need to open the door and turn off the light without spilling the coffee or dropping the cake.
Chances are that by using your elbows for these tasks, while stabilizing the mug and plate parallel to the floor with your hands, you might succeed in enjoying your coffee break.
\Textcite{Won2015} demonstrated that such configurations of \gls{dfs} could even be used to control an additional virtual third arm.

\Textcite{bernstein1967} discovered that repetitive motor tasks of multi-joint limbs vary in the manner of their execution between repetitions while retaining consistent performance at the task goal.
He coined this observation \textquote{repetition without repetition}.
The classical example is the expert blacksmith who is hitting a chisel with a hammer.
\Citeauthor{bernstein1967} observed that there is more variability in the trajectories of the individual joints across repetitions than in the trajectories of the tip of the tool and its final state.
\Cref{fig:blacksmith} illustrates this phenomenon.
The question of how the \gls{cns} arrives at a unique solution to a movement task given the large number of degrees of freedom and infinite possible configurations became known as the problem of \gls{mr}.
%
\begin{figure}[!ht]
  \begin{center}
    \caption{Multiple trajectories in a repetitive task.}
    \includegraphics[width=0.75\textwidth]{Cyclogram_blacksmith.jpg}
    \label{fig:blacksmith}
  \end{center}
  \figurenote{A cyclogram of a blacksmith hitting a chisel with a hammer. Dashed lines represent wrist trajectories of different repetitions. \ccPublicDomain{} Public Domain.}
\end{figure}

Acknowledging that there is uncertainty caused by noise in the environment and the sensorimotor system, a question that concerns researchers in motor control is how the \gls{cns} ensures the successful outcome of a task.
Rather than viewing the variability as a nuisance caused by biological noise, many studies considered researching the structure of variability in redundant systems as an opportunity to gather insights into the underlying control strategies and neural mechanisms \parencite{Campolo2013, Latash2002}.
Such issues are the subject of motor control theories, which are model constructs that try to explain control principles.

Identifying motor control strategies and understanding the relation between the structure of variability and task performance could potentially lead to means of early diagnosis of neuromuscular diseases, like Parkinson's disease or multiple sclerosis \parencite{Park2012,Vaz2019}.
It could further help to design and build efficient and safe wearable robotic systems for rehabilitation of neuromuscular impairments, as well as advance expert and athlete training methods \parencite{Kim2012,DiCesare2020,Moehler2020}.

This study aims to explore the effects of concurrent task goals on task performance and the structure of trial-to-trial variability in a minimally redundant system.
The predictions made by certain motor control theories shall be evaluated in view of the collected data.

A novel approach to neuropsychological research was attempted because a world-wide pandemic made it impossible to research in a familiar laboratory setting with personal contact.
To safely conduct research during this time, a mobile app was developed for the ambulatory data acquisition.

% Provide an overview of the thesis structure.
\bigskip
The remaining part of the thesis proceeds as follows:
An overview of the related theoretical background and the derived research questions is given in the next section.
The third section is concerned with the methodology used for this study.
The findings of the research are presented in the fourth section.
The main body of the study ends with a discussion of the findings.
Additional material is presented as appendixes.

\end{document}