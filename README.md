[![Pipeline status](https://gitlab.com/OlafHaag/thesis/badges/master/pipeline.svg)](https://gitlab.com/OlafHaag/thesis/badges/master/pipeline.svg)
[![Download PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/OlafHaag/thesis/-/jobs/artifacts/master/raw/main.pdf?job=compile)

# Diploma Thesis

## VSCode with LaTeX Workshop

- requires TexLive/MikTeX
- ToDo: explore Docker alternative.

## CMake

Compile locally with

`make clean render`

or

`make clean render LATEXMK_OPTIONS_EXTRA=-pvc` to keep compiling the pdf when the input files are updated.
