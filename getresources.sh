#!/usr/bin/env bash
wget --content-disposition -i ./res/urls.txt
unzip -o \*.zip -d ./res/ -x *.nc *.pkl .gitignore
rm *.zip
