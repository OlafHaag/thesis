require 'pdf/reader'

describe 'LaTeX PDF' do
  reader = PDF::Reader.new('main.pdf')

  #it 'should have 83 pages' do
  #  expect(reader.page_count).to eq(83)
  #end
  #it 'should be titled Effects of Secondary Constraints on Bimanual Two-Digit Synergies in an Accurate Reaching Task' do
  #  expect(reader.info[:Title]).to eq('Effects of Secondary Constraints on Bimanual Two-Digit Synergies in an Accurate Reaching Task')
  #end
  it 'should be made by TeX' do
    expect(reader.info[:Creator]).to eq('LaTeX with hyperref')
  end
  #it 'should be authored by Olaf Haag' do
  #  expect(reader.info[:Author]).to eq('Olaf Haag')
  #end
  #it 'Should be A4 sized' do
  #  expect(reader.pages[0].attributes[:MediaBox]).to eq([0, 0, 595.276, 841.89])
  #end
  #it 'should have 2 fonts on page 1' do
  #  expect(reader.pages[0].fonts.keys.size).to eq(2)
  #end
  #it '42nd page should contain references' do
  #  expect(reader.pages[41].text).to match('References')
  #end
  #it '49th page should contain acronyms' do
  #  expect(reader.pages[48].text).to match('Acronyms')
  #end
end